#include "azn/cpp_lib/array.hpp"
#include "gtest/gtest.h"
#include "type_traits"

using namespace azn;

struct Obj {
	double d;
	int i;
};

bool operator==(const Obj& lhs, const Obj& rhs) {
	return lhs.d == rhs.d && lhs.i == rhs.i;
}

TEST(construct, ar_u8) {
	uint8_t buf[10];
	Array<uint8_t>(buf, 10);
}

TEST(construct, ar_int) {
	int buf[10];
	Array<int>(buf, 10);
}

TEST(construct, ar_obj) {
	Obj buf[10];
	Array<Obj>(buf, 10);
}

TEST(construct, copy_constructor_deleted) {
	EXPECT_FALSE(std::is_copy_constructible<Array<int>>::value);
}

TEST(copy, deleted) {
	EXPECT_FALSE(std::is_copy_assignable<Array<int>>::value);
}

TEST(capacity, ar_u8) {
	uint8_t buf[10];
	Array<uint8_t> ar(buf, 10);
	EXPECT_EQ(ar.capacity(), 10);
}

TEST(capacity, ar_int) {
	int buf[10];
	Array<int> ar(buf, 10);
	EXPECT_EQ(ar.capacity(), 10);
}

TEST(capacity, ar_i32) {
	int32_t buf[12];
	Array<int32_t> ar(buf, 12);
	EXPECT_EQ(ar.capacity(), 12);
}

TEST(size, after_construct) {
	uint8_t buf[10];
	Array<uint8_t> ar(buf, 10);
	EXPECT_EQ(ar.size(), 0);
	EXPECT_TRUE(ar.empty());
	EXPECT_FALSE(ar.full());
}

TEST(size, zero_capacity) {
	uint8_t buf[10];
	Array<uint8_t> ar(buf, 0);
	EXPECT_EQ(ar.size(), 0);
	EXPECT_EQ(ar.capacity(), 0);
	EXPECT_TRUE(ar.empty());
	EXPECT_TRUE(ar.full());
}

TEST(add, into_empty) {
	uint8_t buf[12] = {0};
	Array<uint8_t> ar(buf, 12);
	bool ok = ar.add(3);
	EXPECT_TRUE(ok);
	EXPECT_EQ(buf[0], 3);
}

TEST(add, until_full) {
	uint8_t buf[3] = {0};
	Array<uint8_t> ar(buf, 3);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(buf[0], 3);
	EXPECT_EQ(buf[1], 4);
	EXPECT_EQ(buf[2], 5);
}

TEST(add, into_full) {
	uint8_t buf[3] = {0};
	Array<uint8_t> ar(buf, 3);

	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(buf[0], 3);
	EXPECT_EQ(buf[1], 4);
	EXPECT_EQ(buf[2], 5);
	ok = ok && ar.add(6);
	EXPECT_FALSE(ok);
}

TEST(add, obj) {
	Obj buf[3] = {0};
	Array<Obj> ar(buf, 3);

	bool ok = ar.add({2.0, 1});
	EXPECT_TRUE(ok);
	Obj obj{-3.2, 2};
	ok = ok && ar.add(obj);

	EXPECT_TRUE(ok);
	Obj expected{2.0, 1};
	EXPECT_EQ(buf[0], expected);
	EXPECT_EQ(buf[1], obj);
}

TEST(subscript, read) {
	uint8_t buf[3] = {0};
	Array<uint8_t> ar(buf, 3);
	bool ok = ar.add(3);
	ASSERT_TRUE(ok);
	ASSERT_EQ(buf[0], 3);

	const auto el = ar[0];

	EXPECT_EQ(el, 3);
}

TEST(subscript, write) {
	uint8_t buf[3] = {0};
	Array<uint8_t> ar(buf, 3);
	bool ok = ar.add(3);
	ASSERT_TRUE(ok);
	ASSERT_EQ(buf[0], 3);

	ar[0] = 5;

	EXPECT_EQ(buf[0], 5);
}

TEST(remove, last) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);

	ok = ar.remove(2);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 2);
	EXPECT_EQ(ar[0], 3);
	EXPECT_EQ(ar[1], 4);
}

TEST(remove, invalid_index) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);

	ok = ar.remove(3);

	EXPECT_FALSE(ok);
	EXPECT_EQ(ar.size(), 3);
	EXPECT_EQ(ar[0], 3);
	EXPECT_EQ(ar[1], 4);
	EXPECT_EQ(ar[2], 5);
}

TEST(remove, in_the_middle) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);

	ok = ar.remove(1);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 2);
	EXPECT_EQ(ar[0], 3);
	EXPECT_EQ(ar[1], 5);
}

TEST(remove, first) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);

	ok = ar.remove(0);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 2);
	EXPECT_EQ(ar[0], 4);
	EXPECT_EQ(ar[1], 5);
}

TEST(remove, by_iter) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);

	auto it = ar.begin();
	ok = ar.remove(it);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 2);
	EXPECT_EQ(ar[0], 4);
	EXPECT_EQ(ar[1], 5);
	EXPECT_EQ(it, ar.begin());
}


TEST(clean, ar_u8) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);

	ar.clear();

	EXPECT_EQ(ar.size(), 0);
}

TEST(resize, add_default_to_empty) {
	uint8_t buf[10] = {1, 2, 3, 4, 5, 6, 7, 8};
	Array<uint8_t> ar(buf, 10);
	ASSERT_EQ(ar.size(), 0);
	ASSERT_EQ(ar.capacity(), 10);

	bool ok = ar.resize(5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 0);
	EXPECT_EQ(buf[1], 0);
	EXPECT_EQ(buf[2], 0);
	EXPECT_EQ(buf[3], 0);
	EXPECT_EQ(buf[4], 0);
}

TEST(resize, add_default_to_not_empty) {
	uint8_t buf[10] = {1, 2, 3, 4, 5, 6, 7, 8};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);
	ASSERT_EQ(ar.capacity(), 10);

	ok = ar.resize(5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 1);
	EXPECT_EQ(buf[1], 2);
	EXPECT_EQ(buf[2], 3);
	EXPECT_EQ(buf[3], 0);
	EXPECT_EQ(buf[4], 0);
}

TEST(resize, add_to_empty) {
	uint8_t buf[10] = {1, 2, 3, 4, 5, 6, 7, 8};
	Array<uint8_t> ar(buf, 10);
	ASSERT_EQ(ar.size(), 0);
	ASSERT_EQ(ar.capacity(), 10);

	bool ok = ar.resize(5, 1);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 1);
	EXPECT_EQ(buf[1], 1);
	EXPECT_EQ(buf[2], 1);
	EXPECT_EQ(buf[3], 1);
	EXPECT_EQ(buf[4], 1);
}

TEST(resize, add_to_not_empty) {
	uint8_t buf[10] = {1, 2, 3, 4, 5, 6, 7, 8};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 3);
	ASSERT_EQ(ar.capacity(), 10);

	ok = ar.resize(5, 1);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 1);
	EXPECT_EQ(buf[1], 2);
	EXPECT_EQ(buf[2], 3);
	EXPECT_EQ(buf[3], 1);
	EXPECT_EQ(buf[4], 1);
}
TEST(resize, remove_from_full) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ok = ok && ar.add(6);
	ok = ok && ar.add(7);
	ok = ok && ar.add(8);
	ok = ok && ar.add(9);
	ok = ok && ar.add(0);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 10);
	ASSERT_EQ(ar.capacity(), 10);

	ok = ar.resize(5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 1);
	EXPECT_EQ(buf[1], 2);
	EXPECT_EQ(buf[2], 3);
	EXPECT_EQ(buf[3], 4);
	EXPECT_EQ(buf[4], 5);
}

TEST(resize, remove_from_not_full) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	ok = ok && ar.add(6);
	ok = ok && ar.add(7);
	ok = ok && ar.add(8);
	ASSERT_TRUE(ok);
	ASSERT_EQ(ar.size(), 8);
	ASSERT_EQ(ar.capacity(), 10);

	ok = ar.resize(5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 1);
	EXPECT_EQ(buf[1], 2);
	EXPECT_EQ(buf[2], 3);
	EXPECT_EQ(buf[3], 4);
	EXPECT_EQ(buf[4], 5);
}


TEST(insert, into_empty_at_0) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);

	bool ok = ar.insert(0, 3);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 1);
	EXPECT_EQ(buf[0], 3);
}

TEST(insert, into_empty_at_1) {
	uint8_t buf[10] = {0};
	Array<uint8_t> ar(buf, 10);

	bool ok = ar.insert(1, 3);

	EXPECT_FALSE(ok);
}

TEST(insert, into_full) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ok = ok && ar.add(4);
	ok = ok && ar.add(5);
	EXPECT_TRUE(ok);

	ok = ar.insert(1, 3);

	EXPECT_FALSE(ok);
}

TEST(insert, into_almost_full) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ok = ok && ar.add(4);
	EXPECT_TRUE(ok);

	ok = ar.insert(4, 5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[4], 5);
}

TEST(insert, into_begin) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ok = ok && ar.add(4);
	EXPECT_TRUE(ok);

	ok = ar.insert(0, 5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 5);
	EXPECT_EQ(buf[1], 1);
	EXPECT_EQ(buf[2], 2);
	EXPECT_EQ(buf[3], 3);
	EXPECT_EQ(buf[4], 4);
}

TEST(insert, into_middle) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ok = ok && ar.add(4);
	EXPECT_TRUE(ok);

	ok = ar.insert(2, 5);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(buf[0], 1);
	EXPECT_EQ(buf[1], 2);
	EXPECT_EQ(buf[2], 5);
	EXPECT_EQ(buf[3], 3);
	EXPECT_EQ(buf[4], 4);
}

TEST(insert, iterator_begin) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);

	auto it = ar.begin();
	bool ok = ar.insert(it, 2);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 1);
	EXPECT_EQ(ar[0], 2);

}

TEST(insert, iterator_end) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);

	auto it = ar.end();
	bool ok = ar.insert(it, 2);

	EXPECT_TRUE(ok);
	EXPECT_EQ(ar.size(), 1);
	EXPECT_EQ(ar[0], 2);
	EXPECT_EQ(it, ar.end());
}

TEST(insert, iterator_into_full) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);

	auto it = ar.end();
	bool ok = ar.insert(it, 2);
	ok = ok && ar.insert(it, 2);
	ok = ok && ar.insert(it, 2);
	ok = ok && ar.insert(it, 2);
	ok = ok && ar.insert(it, 2);
	ASSERT_TRUE(ok);

	ok = ok && ar.insert(it, 2);

	EXPECT_FALSE(ok);
	EXPECT_EQ(ar.size(), 5);
	EXPECT_EQ(it, ar.end());
}

TEST(iterators, empty) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	EXPECT_EQ(ar.begin(), ar.end());
}

TEST(iterators, inc) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ASSERT_TRUE(ok);

	auto it = ar.begin();
	EXPECT_EQ(*it, 1);

	++it;
	EXPECT_EQ(*it, 2);

	++it;
	EXPECT_EQ(it, ar.end());
}

TEST(iterators, dec) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(1);
	ok = ok && ar.add(2);
	ASSERT_TRUE(ok);

	auto it = ar.end();
	--it;
	EXPECT_EQ(*it, 2);

	--it;
	EXPECT_EQ(*it, 1);
	EXPECT_EQ(it, ar.begin());
}

TEST(iterators, fange_for_loop) {
	uint8_t buf[5] = {0};
	Array<uint8_t> ar(buf, 5);
	bool ok = ar.add(0);
	ok = ok && ar.add(1);
	ok = ok && ar.add(2);
	ok = ok && ar.add(3);
	ASSERT_TRUE(ok);

	for(auto& el : ar) {
		el = 8;
	}

	EXPECT_EQ(ar[0], 8);
	EXPECT_EQ(ar[1], 8);
	EXPECT_EQ(ar[2], 8);
	EXPECT_EQ(ar[3], 8);
}
