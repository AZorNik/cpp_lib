#include "azn/cpp_lib/ringBuffer.hpp"
#include "gtest/gtest.h"

using namespace azn;

struct Obj {
	double d;
	int i;
};

struct CountedObj {
	CountedObj(void) { countConstruct++;}
	CountedObj(CountedObj&& other)
			:	d{std::move(other.d)}
			, i{std::move(other.i)} {
		countConstruct++;
	}
	CountedObj(const CountedObj& other)
			:	d{other.d}
			, i{other.i} {
		countConstruct++;
	}
	~CountedObj(void) { countDistruct++; }

	CountedObj& operator=(const CountedObj& other) = default;
	CountedObj& operator=(CountedObj&& other) = default;

	double d {0.0};
	int i{0};

	static std::size_t countConstruct;
	static std::size_t countDistruct;
};

std::size_t CountedObj::countConstruct = 0;
std::size_t CountedObj::countDistruct = 0;

bool operator==(const Obj& lhs, const Obj& rhs) {
	return lhs.d == rhs.d && lhs.i == rhs.i;
}

TEST(construct, ar_u8) {
	uint8_t buf[10];
	RingBuffer<uint8_t>(buf, 10);
}

TEST(construct, ar_char) {
	unsigned char buf[10];
	RingBuffer<unsigned char>(buf, 10);
}

TEST(construct, ar_int) {
	int buf[10];
	RingBuffer<int>(buf, 10);
}

TEST(construct, ar_obj) {
	Obj buf[10];
	RingBuffer<Obj>(buf, 10);
}

TEST(construct, buf_of_bytes) {
	CountedObj::countConstruct = 0;
	CountedObj::countDistruct = 0;

	uint8_t buf[sizeof(CountedObj)*10];
	RingBuffer<CountedObj> rb(buf, sizeof(buf));

	EXPECT_EQ(CountedObj::countConstruct, 0);
	EXPECT_EQ(rb.capacity(), 10);
}

TEST(construct, copy_constructor_deleted) {
	EXPECT_FALSE(std::is_copy_constructible_v<RingBuffer<int>>);
}

TEST(copy, deleted) {
	EXPECT_FALSE(std::is_copy_assignable_v<RingBuffer<int>>);
}

TEST(capacity, ar_u8) {
	uint8_t buf[10];
	RingBuffer<uint8_t> rb(buf, 10);
	EXPECT_EQ(rb.capacity(), 10);
}

TEST(capacity, ar_int) {
	int buf[12];
	RingBuffer<int> rb(buf, 10);
	EXPECT_EQ(rb.capacity(), 10);
}

TEST(write, by_lref) {
	int buf[12];
	RingBuffer<int> rb(buf, 12);
	ASSERT_EQ(rb.capacity(), 12);

	int el = 8;
	bool ok = rb.write(el);
	EXPECT_TRUE(ok);
	EXPECT_EQ(buf[0], el);
}

TEST(write, by_rref) {
	int buf[12];
	RingBuffer<int> rb(buf, 12);
	ASSERT_EQ(rb.capacity(), 12);

	bool ok = rb.write(8);
	EXPECT_TRUE(ok);
	EXPECT_EQ(buf[0], 8);
}

TEST(write, _2_elements) {
	int buf[12];
	RingBuffer<int> rb(buf, 12);
	ASSERT_EQ(rb.capacity(), 12);

	bool ok = rb.write(8);
	EXPECT_TRUE(ok);
	ok = rb.write(9);
	EXPECT_TRUE(ok);

	EXPECT_EQ(buf[0], 8);
	EXPECT_EQ(buf[1], 9);
}

TEST(write, into_full) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	ASSERT_EQ(rb.capacity(), 3);
	bool ok = rb.write(8);
	ok = ok && rb.write(8);
	ok = ok && rb.write(8);
	ASSERT_TRUE(ok);

	ok = rb.write(4);
	EXPECT_FALSE(ok);
}

TEST(write, full_read_write) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	ASSERT_EQ(rb.capacity(), 3);
	bool ok = rb.write(8);
	ok = ok && rb.write(8);
	ok = ok && rb.write(8);
	int i;
	ok = ok && rb.read(i);
	ASSERT_TRUE(ok);

	ok = rb.write(8);
	EXPECT_TRUE(ok);
}


TEST(read, from_empty) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	ASSERT_EQ(rb.capacity(), 3);

	int i;
	bool ok = rb.read(i);

	EXPECT_FALSE(ok);
}

TEST(read, from_buf_with_1_element) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	bool ok = rb.write(8);
	ASSERT_TRUE(ok);

	int i;
	ok = rb.read(i);
	EXPECT_TRUE(ok);
	EXPECT_EQ(i, 8);
}

TEST(read, from_buf_with_2_element) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	bool ok = rb.write(8);
	ok = ok && rb.write(9);
	ASSERT_TRUE(ok);

	int i;
	ok = rb.read(i);
	EXPECT_TRUE(ok);
	EXPECT_EQ(i, 8);

	ok = rb.read(i);
	EXPECT_TRUE(ok);
	EXPECT_EQ(i, 9);
}

TEST(read, destroy_obj) {

	CountedObj::countConstruct = 0;
	CountedObj::countDistruct = 0;
	uint8_t  buf[sizeof(CountedObj)*10];
	RingBuffer<CountedObj> rb(buf, 10);

	bool ok = rb.write({});
	ASSERT_TRUE(ok);
	EXPECT_EQ(CountedObj::countConstruct, 2);
	EXPECT_EQ(CountedObj::countDistruct, 1);

	{
		CountedObj obj;
		EXPECT_EQ(CountedObj::countConstruct, 3);
		EXPECT_EQ(CountedObj::countDistruct, 1);
		ok = rb.read(obj);
		ASSERT_TRUE(ok);
		EXPECT_EQ(CountedObj::countConstruct, 3);
		EXPECT_EQ(CountedObj::countDistruct, 2);

	}

	EXPECT_EQ(CountedObj::countConstruct, 3);
	EXPECT_EQ(CountedObj::countDistruct, 3);
}

TEST(size, empty) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	ASSERT_TRUE(rb.isEmty());

	EXPECT_EQ(rb.size(), 0);
}

TEST(size, after_write) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);

	auto sizeBefore = rb.size();
	bool ok = rb.write(8);
	ASSERT_TRUE(ok);

	EXPECT_EQ(rb.size(), sizeBefore + 1);
}

TEST(size, after_read) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	bool ok = rb.write(8);
	auto sizeBefore = rb.size();

	int i;
	ok = ok && rb.read(i);

	EXPECT_TRUE(ok);
	EXPECT_EQ(rb.size(), sizeBefore - 1);
}

TEST(size, full) {
	int buf[3];
	RingBuffer<int> rb(buf, 3);
	bool ok = rb.write(8);
	ok = ok && rb.write(8);
	ok = ok && rb.write(8);
	ASSERT_TRUE(ok);
	ASSERT_TRUE(rb.isFull());

	EXPECT_EQ(rb.size(), 3);
}
