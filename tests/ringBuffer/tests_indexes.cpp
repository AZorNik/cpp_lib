#include "azn/cpp_lib/ringBuffer.hpp"
#include "gtest/gtest.h"

using Indexes = azn::ringBuffer::Indexes;

TEST(indexes, isEmpty) {
	Indexes i(10);
	i.write = 5;
	i.read = i.write;
	i.writeIndexLeft = false;

	EXPECT_TRUE(i.isEmpty());
}


TEST(indexes, isNotEmpty) {
	Indexes i(10);
	i.write = 5;
	i.read = i.write - 1;
	i.writeIndexLeft = false;

	EXPECT_FALSE(i.isEmpty());
}

TEST(indexes, isFull) {
	Indexes i(10);
	i.write = 5;
	i.read = i.write;
	i.writeIndexLeft = true;

	EXPECT_TRUE(i.isFull());
}

TEST(indexes, isNotFull) {
	Indexes i(10);
	i.write = 5;
	i.read = i.write - 1;
	i.writeIndexLeft = false;

	EXPECT_FALSE(i.isFull());
}

TEST(indexes, incIndex) {
	Indexes i(10);
	i.write = 0;
	i.incIndex(i.write);

	EXPECT_EQ(i.write, 1);
}

TEST(indexes, incIndexOverFlow) {
	Indexes i(10);
	i.write = 9;
	i.incIndex(i.write);

	EXPECT_EQ(i.write, 0);
}

TEST(indexes, incReadNotEmpty) {
	Indexes i(10);
	i.writeIndexLeft = true;
	ASSERT_FALSE(i.isEmpty());

	auto before = i.read;
	i.incReadIndex();
	i.incIndex(before);

	EXPECT_EQ(i.read, before);
}

TEST(indexes, incReadEmpty) {
	Indexes i(10);
	ASSERT_TRUE(i.isEmpty());

	auto before = i.read;
	i.incReadIndex();

	EXPECT_EQ(i.read, before);
}

TEST(indexes, incWriteNotFull) {
	Indexes i(10);
	ASSERT_FALSE(i.isFull());

	auto before = i.write;
	i.incWriteIndex();
	i.incIndex(before);

	EXPECT_EQ(i.write, before);
}

TEST(indexes, incWriteFull) {
	Indexes i(10);
	i.writeIndexLeft = true;
	ASSERT_TRUE(i.isFull());

	auto before = i.write;
	i.incWriteIndex();

	EXPECT_EQ(i.write, before);
}

TEST(indexes, setWriteIndexLeft) {
	Indexes i(10);
	i.write = 9;
	i.writeIndexLeft = false;

	i.incWriteIndex();
	EXPECT_TRUE(i.writeIndexLeft);
}


TEST(indexes, resetWriteIndexLeft) {
	Indexes i(10);
	i.read= 9;
	i.writeIndexLeft = true;

	i.incReadIndex();
	EXPECT_FALSE(i.writeIndexLeft);
}


TEST(indexes_size, full) {
	Indexes i(10);
	i.read = 4;
	i.write = i.read;
	i.writeIndexLeft = true;

	EXPECT_EQ(i.size(), 10);
}

TEST(indexes_size, empty) {
	Indexes i(10);
	i.read = 4;
	i.write = i.read;
	i.writeIndexLeft = false;

	EXPECT_EQ(i.size(), 0);
}

TEST(indexes_size, writeIndexLeft) {
	Indexes i(10);
	i.read = 4;
	i.write = i.read - 3;
	i.writeIndexLeft = true;

	EXPECT_EQ(i.size(), 7);
}


TEST(indexes_size, writeIndexRight) {
	Indexes i(10);
	i.read = 4;
	i.write = i.read + 2;
	i.writeIndexLeft = false;

	EXPECT_EQ(i.size(), 2);
}
