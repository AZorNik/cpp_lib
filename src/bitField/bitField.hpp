#pragma once

#include <cstddef>

namespace azn {

template<typename T, typename U = T>
struct BitField {
	const std::size_t pos;
	const T mask;

	static constexpr T bitsSeq(std::size_t n) {
		T t = 0;
		while (n-- > 0){
			t = (t << 1) + 1;
		}
		return t;
	}

	constexpr BitField(std::size_t bitsQty, std::size_t pos) : pos(pos), mask(bitsSeq(bitsQty) << pos) {}
	constexpr T value(T val) const { return val << pos; }
	constexpr T apply(T val) const { return (val & mask) >> pos; }
	constexpr bool match(T value, U ref) const { return apply(value) == static_cast<T>(ref); }
	constexpr void setField(T& dest, U value) const;
	constexpr U getField(T val) const { return static_cast<U>(apply(val)); }
};


template<typename T, typename U>
constexpr void BitField<T,U>::setField(T& dest, U value) const {
	dest &= ~(mask);
	dest |= (static_cast<T>(value) & (mask >> pos)) << pos;
}

}

