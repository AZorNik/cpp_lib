#pragma once

#include <cassert>
#include <cstdint>
#include <utility>
#include "../bunch.hpp"

namespace azn {

template<typename T>
class Array {
public:
	explicit Array(const Bunch<T>& buffer) noexcept
			: buffer(buffer)
			, dataSize(0){
	}

	template<typename U=T, std::enable_if_t<!std::is_same_v<U, uint8_t>, bool> = true>
	Array(uint8_t* buf, std::size_t size) noexcept
			: buffer({reinterpret_cast<T*>(buf), size / sizeof(T)})
			, dataSize(0) {

	}

	Array(T* pointer, std::size_t size) noexcept
			: buffer({pointer, size})
			, dataSize(0) {

	}

	virtual ~Array(void);

	using iterator = T*;
	using const_iterator = const T*;

	Array(const Array&) = delete;
	Array operator=(const Array&) = delete;

	std::size_t capacity(void) const noexcept { return buffer.size; }
	std::size_t size(void) const noexcept { return dataSize; }
	bool empty(void) const noexcept { return size() == 0; }
	bool full(void) const noexcept { return size() == capacity(); }

	[[nodiscard]] bool add(const T& el) noexcept;
	[[nodiscard]] bool add(T&& el) noexcept;
	[[nodiscard]] bool insert(std::size_t i, const T& el) noexcept;
	[[nodiscard]] bool insert(std::size_t i, T&& el) noexcept;
	[[nodiscard]] bool insert(iterator& i, const T& el) noexcept;
	[[nodiscard]] bool insert(iterator& i, T&& el) noexcept;
	[[nodiscard]] bool remove(std::size_t i) noexcept;
	[[nodiscard]] bool remove(iterator& it) noexcept;
	void clear(void) noexcept;
	[[nodiscard]] bool resize(std::size_t newSize, const T& el) noexcept;
	[[nodiscard]] bool resize(std::size_t newSize) noexcept { return resize(newSize, T());}

	T& operator[](std::size_t i) noexcept;
	const T& operator[](std::size_t i) const noexcept;

	iterator begin(void) noexcept { return buffer.pointer; }
	const_iterator begin(void) const noexcept { return buffer.pointer; }
	const_iterator cbegin(void) const noexcept { return buffer.pointer; }
	iterator end(void) noexcept { return buffer.pointer + dataSize; }
	const_iterator end(void) const noexcept { return buffer.pointer + dataSize; }
	const_iterator cend(void) const noexcept { return buffer.pointer + dataSize; }

private:
	Bunch<T> buffer {nullptr, 0};
	std::size_t dataSize {0};
};

template<typename T>
Array<T>::~Array(void) {
	clear();
}

template<typename T>
void Array<T>::clear(void) noexcept {
	while(dataSize > 0) {
		buffer[dataSize].~T();
		--dataSize;
	}
}

template<typename T>
bool Array<T>::add(const T& el) noexcept {
	bool ok = !full();
	if (ok) {
		new(buffer.pointer + dataSize) T(el);
		++dataSize;
	}

	return ok;
}

template<typename T>
bool Array<T>::add(T&& el) noexcept {
	bool ok = !full();
	if (ok) {
		new(buffer.pointer + dataSize) T(std::move(el));
		++dataSize;
	}

	return ok;
}

template<typename T>
bool Array<T>::remove(std::size_t i) noexcept {
	bool ok = i < dataSize;
	if (ok) {
		buffer[i].~T();

		for(; i < (dataSize - 1); i++){
			std::swap(buffer[i], buffer[i + 1]);
		}
		--dataSize;
	}

	return ok;
}

template<typename T>
bool Array<T>::remove(iterator& it) noexcept {
	return remove(it - begin());
}

template<typename T>
T& Array<T>::operator[](std::size_t i) noexcept {
#ifdef AZN_ARRAY_CHECK_BOUNDARY
	assert(i < dataSize);
#endif
	return *(buffer.pointer + i);
}

template<typename T>
const T& Array<T>::operator[](std::size_t i) const noexcept {
#ifdef AZN_ARRAY_CHECK_BOUNDARY
	assert(i < dataSize);
#endif
	return *(buffer.pointer + i);
}

template<typename T>
bool Array<T>::resize(std::size_t newSize, const T& el) noexcept {
	bool ok = newSize < capacity();

	if (ok) {
		while (dataSize > newSize) {
			ok = ok && remove(dataSize - 1);
		}
		while (dataSize < newSize) {
			ok = ok && add(el);
		}
	}

	return ok;
}

template<typename T>
bool Array<T>::insert(std::size_t i, const T& el) noexcept {
	bool ok = !full() && (i <= dataSize);

	if (ok) {
		ok = ok && add(el);
		auto j = dataSize - 1;
		while (j > i) {
			std::swap(buffer[j], buffer[j - 1]);
			--j;
		}
	}

	return ok;
}

template<typename T>
bool Array<T>::insert(std::size_t i, T&& el) noexcept {
	bool ok = !full() && (i <= dataSize);

	if (ok) {
		ok = ok && add(std::move(el));
		auto j = dataSize - 1;
		while (j > i) {
			std::swap(buffer[j], buffer[j - 1]);
			--j;
		}
	}

	return ok;
}

template<typename T>
bool Array<T>::insert(iterator& it, const T& el) noexcept {
	bool ok = insert(it - begin(), el);
	if (ok) {
		++it;
	}
	return ok;
}

template<typename T>
bool Array<T>::insert(iterator& it, T&& el) noexcept {
	bool ok = insert(it - begin(), std::move(el));
	if (ok) {
		++it;
	}
	return ok;
}

} // namespace azn
