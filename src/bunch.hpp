#pragma once

#include <cstddef>

namespace azn {

template<typename T>
struct Bunch {
	T* pointer;
	std::size_t size;
	Bunch(T* pointer, std::size_t size) : pointer(pointer), size(size) {}
	T& operator[](std::size_t i) noexcept { return *(pointer + i); }
	const T& operator[](std::size_t i) const noexcept { return *(pointer + i); }
};

}
