#pragma once

#include <cstdint>
#include <utility>
#include <new>
#include <type_traits>
#include "indexes.hpp"
#include "../bunch.hpp"

namespace azn {

template<typename T>
class RingBuffer {
public:
	explicit RingBuffer(const Bunch<T>& buffer) noexcept
			: buffer(buffer)
			, indexes(buffer.size){
	}

	template<typename U=T, std::enable_if_t<!std::is_same_v<U, uint8_t>, bool> = true>
	RingBuffer(uint8_t* buf, std::size_t size) noexcept
			: buffer({reinterpret_cast<T*>(buf), size / sizeof(T)})
			, indexes(buffer.size) {

	}

	RingBuffer(T* pointer, std::size_t size) noexcept
			: buffer({pointer, size})
			, indexes(buffer.size) {

	}

	~RingBuffer(void) {}

	RingBuffer(const RingBuffer&) = delete;
	RingBuffer operator=(const RingBuffer&) = delete;

	std::size_t capacity(void) const noexcept { return buffer.size; }
	[[nodiscard]] bool write(const T& t) noexcept;
	[[nodiscard]] bool write(T&& t) noexcept;
	[[nodiscard]] bool read(T& t) noexcept;
	[[nodiscard]] bool isEmty(void) const noexcept { return indexes.isEmpty(); }
	[[nodiscard]] bool isFull(void) const noexcept { return indexes.isFull(); }
	[[nodiscard]] std::size_t size(void) const noexcept { return indexes.size(); }

private:
	Bunch<T> buffer {nullptr, 0};
	ringBuffer::Indexes indexes;
};


template<typename T>
bool RingBuffer<T>::write(const T& t) noexcept {
	bool ok = !indexes.isFull();

	if (ok) {
		new(&buffer[indexes.write]) T(t);
		indexes.incWriteIndex();
	}

	return ok;
}

template<typename T>
bool RingBuffer<T>::write(T&& t) noexcept {
	bool ok = !indexes.isFull();

	if (ok) {
		new(&buffer[indexes.write]) T(std::move(t));
		indexes.incWriteIndex();
	}

	return ok;
}

template<typename T>
bool RingBuffer<T>::read(T& t) noexcept {
	bool ok = !indexes.isEmpty();

	if (ok) {
		t = std::move(buffer[indexes.read]);
		buffer[indexes.read].~T();
		indexes.incReadIndex();
	}

	return ok;
}

} // namespace azn
