#pragma once
#include <cstdint>

namespace azn::ringBuffer {

struct Indexes {
	explicit Indexes(std::size_t max) : maxVal(max) {}

	const std::size_t maxVal;
	std::size_t write {0};
	std::size_t read {0};
	bool writeIndexLeft {false};

	[[nodiscard]] bool incIndex(std::size_t& index) noexcept;
	[[nodiscard]] bool isEmpty(void) const noexcept;
	[[nodiscard]] bool isFull(void) const noexcept;
	void incWriteIndex(void) noexcept;
	void incReadIndex(void) noexcept;

	[[nodiscard]] std::size_t size(void) const noexcept;

};

} // namespace azn
