#include "indexes.hpp"

using Rbi = azn::ringBuffer::Indexes;

bool Rbi::incIndex(std::size_t& index) noexcept {
	bool outOfRange;
	if (++index >= maxVal) {
		index = 0;
		outOfRange = true;
	}
	else {
		outOfRange = false;
	}

	return outOfRange;
}

bool Rbi::isEmpty(void) const noexcept {
	return (write == read) && !writeIndexLeft;
}

bool Rbi::isFull(void) const noexcept {
	return (write == read) && writeIndexLeft;
}

void Rbi::incWriteIndex(void) noexcept {
	if (!isFull()){
		if (incIndex(write)) {
			writeIndexLeft = true;
		}
	}
}

void Rbi::incReadIndex(void) noexcept {
	if (!isEmpty()){
		if (incIndex(read)) {
			writeIndexLeft = false;
		}
	}
}

std::size_t Rbi::size(void) const noexcept {
	return writeIndexLeft ? maxVal - read + write : write - read;
}
